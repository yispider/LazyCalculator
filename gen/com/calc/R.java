/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.calc;

public final class R {
    public static final class array {
        public static final int app_calc_round_mode_list=0x7f050002;
        public static final int app_calc_round_mode_list_value=0x7f050003;
        public static final int app_calc_triangle_input_list=0x7f050004;
        public static final int app_calc_triangle_input_list_value=0x7f050005;
        public static final int app_component_list=0x7f050000;
        public static final int app_component_list_value=0x7f050001;
        /**  长度单位 
         */
        public static final int app_length_calc_unit_chose=0x7f050006;
        public static final int app_length_calc_unit_chose_value=0x7f050007;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int index_text_color_black=0x7f060000;
        public static final int index_text_color_white=0x7f060001;
    }
    public static final class drawable {
        public static final int about=0x7f020000;
        public static final int back=0x7f020001;
        public static final int bg_white=0x7f020002;
        public static final int bg_wood=0x7f020003;
        public static final int binary=0x7f020004;
        public static final int change=0x7f020005;
        public static final int contact=0x7f020006;
        public static final int down=0x7f020007;
        public static final int empty=0x7f020008;
        public static final int exchange=0x7f020009;
        public static final int exit=0x7f02000a;
        public static final int general_calculator_text_bg=0x7f02000b;
        public static final int help=0x7f02000c;
        public static final int history=0x7f02000d;
        public static final int icon=0x7f02000e;
        public static final int imcome_tax=0x7f02000f;
        public static final int index=0x7f020010;
        public static final int junior_calculator=0x7f020011;
        public static final int loan=0x7f020012;
        public static final int measure_area=0x7f020013;
        public static final int measure_length=0x7f020014;
        public static final int senior_calculator=0x7f020015;
        public static final int setting=0x7f020016;
    }
    public static final class id {
        public static final int answer=0x7f0a000a;
        public static final int backspace=0x7f0a000e;
        public static final int calc_btn=0x7f0a0003;
        public static final int cos=0x7f0a0010;
        public static final int divide=0x7f0a0016;
        public static final int equal=0x7f0a0022;
        public static final int factorial=0x7f0a000d;
        public static final int icon_index=0x7f0a0005;
        public static final int length_calc_output_area=0x7f0a0004;
        public static final int log=0x7f0a0012;
        public static final int main_grid_view=0x7f0a0009;
        public static final int menu_about=0x7f0a0028;
        public static final int menu_contact=0x7f0a0029;
        public static final int menu_exit=0x7f0a0027;
        public static final int menu_help=0x7f0a0026;
        public static final int menu_setting=0x7f0a0025;
        public static final int minus=0x7f0a001e;
        public static final int multiply=0x7f0a001a;
        public static final int num0=0x7f0a001f;
        public static final int num1=0x7f0a001b;
        public static final int num2=0x7f0a001c;
        public static final int num3=0x7f0a001d;
        public static final int num4=0x7f0a0017;
        public static final int num5=0x7f0a0018;
        public static final int num6=0x7f0a0019;
        public static final int num7=0x7f0a0013;
        public static final int num8=0x7f0a0014;
        public static final int num9=0x7f0a0015;
        public static final int plus=0x7f0a0021;
        public static final int point=0x7f0a0020;
        public static final int power=0x7f0a000b;
        public static final int power_root=0x7f0a000c;
        public static final int sin=0x7f0a000f;
        public static final int soft_version=0x7f0a0000;
        public static final int src_area=0x7f0a0001;
        public static final int src_length=0x7f0a0007;
        public static final int start_layout=0x7f0a0008;
        public static final int tan=0x7f0a0011;
        public static final int text_index=0x7f0a0006;
        public static final int tt_content=0x7f0a0024;
        public static final int tt_label=0x7f0a0023;
        public static final int unit=0x7f0a0002;
    }
    public static final class layout {
        public static final int about=0x7f030000;
        public static final int area_calc=0x7f030001;
        public static final int contact=0x7f030002;
        public static final int image_up_text_down=0x7f030003;
        public static final int length_calc=0x7f030004;
        public static final int main=0x7f030005;
        public static final int senior_calculator=0x7f030006;
        public static final int text_left_text_right=0x7f030007;
    }
    public static final class menu {
        public static final int main_menu=0x7f090000;
    }
    public static final class string {
        public static final int about_introduce=0x7f070026;
        /**  about 
         */
        public static final int about_introduce_title=0x7f070025;
        public static final int app_area_calc=0x7f070015;
        public static final int app_area_calc_value=0x7f070016;
        public static final int app_binary_calc=0x7f07001d;
        public static final int app_binary_calc_value=0x7f07001e;
        public static final int app_calc_round_mode_down=0x7f070041;
        public static final int app_calc_round_mode_half_down=0x7f07003f;
        /**  舍入方式 
         */
        public static final int app_calc_round_mode_half_up=0x7f07003d;
        public static final int app_calc_round_mode_up=0x7f070043;
        public static final int app_calc_round_mode_value_down=0x7f070042;
        public static final int app_calc_round_mode_value_half_down=0x7f070040;
        public static final int app_calc_round_mode_value_half_up=0x7f07003e;
        public static final int app_calc_round_mode_value_up=0x7f070044;
        public static final int app_calc_scale_value=0x7f070038;
        public static final int app_calc_triangle_cnt_input_type_degree=0x7f070047;
        /**  三角函数输入类型
         */
        public static final int app_calc_triangle_cnt_input_type_num=0x7f070045;
        public static final int app_calc_triangle_cnt_input_type_value_degree=0x7f070048;
        public static final int app_calc_triangle_cnt_input_type_value_num=0x7f070046;
        public static final int app_exchange_rate=0x7f07001b;
        public static final int app_exchange_rate_value=0x7f07001c;
        /**  component 
         */
        public static final int app_index=0x7f07000d;
        public static final int app_index_value=0x7f07000e;
        public static final int app_junior_calc=0x7f07000f;
        public static final int app_junior_calc_value=0x7f070010;
        public static final int app_length_calc=0x7f070013;
        public static final int app_length_calc_value=0x7f070014;
        public static final int app_loan_calc=0x7f070017;
        public static final int app_loan_calc_value=0x7f070018;
        public static final int app_name=0x7f070000;
        public static final int app_senior_calc=0x7f070011;
        public static final int app_senior_calc_value=0x7f070012;
        public static final int app_tax_calc=0x7f070019;
        public static final int app_tax_calc_value=0x7f07001a;
        public static final int area_hint=0x7f070066;
        public static final int backspace=0x7f070005;
        public static final int button_calc=0x7f07000c;
        public static final int button_cancle=0x7f07000b;
        public static final int button_ok=0x7f07000a;
        public static final int clear=0x7f070002;
        /**  contact 
         */
        public static final int contact_author_title=0x7f070029;
        public static final int contact_author_way=0x7f07002a;
        public static final int double_back_exit=0x7f070006;
        public static final int hello=0x7f070001;
        public static final int i_know=0x7f070007;
        public static final int input_empty=0x7f070009;
        public static final int input_error=0x7f070008;
        public static final int length_calc_centimeter_name=0x7f07004c;
        public static final int length_calc_chi_name=0x7f070051;
        public static final int length_calc_cun_name=0x7f070052;
        public static final int length_calc_decimeter_name=0x7f07004b;
        public static final int length_calc_fen_name=0x7f070053;
        public static final int length_calc_fu_long_name=0x7f070058;
        public static final int length_calc_hai_li_name=0x7f070055;
        public static final int length_calc_kilometer_name=0x7f07004a;
        public static final int length_calc_li3_name=0x7f070054;
        public static final int length_calc_li_name=0x7f07004f;
        public static final int length_calc_ma_name=0x7f070059;
        /** 长度转换器 设置 
         */
        public static final int length_calc_menu_setting=0x7f07005d;
        /**  长度转换器 
         */
        public static final int length_calc_meter_name=0x7f070049;
        public static final int length_calc_micrometer_name=0x7f07004e;
        public static final int length_calc_millimeter_name=0x7f07004d;
        public static final int length_calc_scale_value=0x7f070060;
        public static final int length_calc_summary_dest_unit=0x7f070064;
        public static final int length_calc_summary_result_scale=0x7f07005f;
        public static final int length_calc_summary_src_unit=0x7f070062;
        public static final int length_calc_title_1_dest_unit=0x7f070063;
        public static final int length_calc_title_1_result_scale=0x7f07005e;
        public static final int length_calc_title_1_src_unit=0x7f070061;
        public static final int length_calc_unit_spinner_prompt=0x7f07005c;
        public static final int length_calc_ying_chi_name=0x7f07005a;
        public static final int length_calc_ying_cun_name=0x7f07005b;
        public static final int length_calc_ying_li_name=0x7f070057;
        public static final int length_calc_ying_xun_name=0x7f070056;
        public static final int length_calc_zhang_name=0x7f070050;
        /**  hint 
         */
        public static final int length_hint=0x7f070065;
        public static final int menu_about=0x7f070023;
        public static final int menu_contact=0x7f070024;
        public static final int menu_exit=0x7f070021;
        public static final int menu_help=0x7f070020;
        /**  menu 
         */
        public static final int menu_setting=0x7f07001f;
        public static final int menu_soft_history=0x7f070022;
        public static final int pref_summary_2_triangle_cnt_input_type=0x7f07003c;
        public static final int pref_summary_calc_round_mode=0x7f07003a;
        public static final int pref_summary_calc_scale=0x7f070037;
        public static final int pref_summary_click_vibrate=0x7f070033;
        public static final int pref_summary_user_index=0x7f070035;
        public static final int pref_title_1_action=0x7f07002f;
        public static final int pref_title_1_apperance=0x7f07002e;
        public static final int pref_title_1_calc_cnt=0x7f070030;
        public static final int pref_title_1_misc=0x7f070031;
        /**  设置页面  
         */
        public static final int pref_title_1_notifications=0x7f07002d;
        public static final int pref_title_2_calc_round_mode=0x7f070039;
        public static final int pref_title_2_calc_scale=0x7f070036;
        public static final int pref_title_2_click_vibrate=0x7f070032;
        public static final int pref_title_2_index=0x7f070034;
        public static final int pref_title_2_triangle_cnt_input_type=0x7f07003b;
        public static final int return_last_activity=0x7f070004;
        public static final int return_to_index=0x7f070003;
        public static final int soft_version=0x7f070028;
        public static final int soft_version_title=0x7f070027;
        /**  update log 
         */
        public static final int update_log_title=0x7f07002b;
        public static final int v_0_1_001=0x7f07002c;
    }
    public static final class style {
        public static final int length_calc_output_label_style=0x7f080000;
    }
    public static final class xml {
        public static final int area_calc_preferences=0x7f040000;
        public static final int general_calc_preferences=0x7f040001;
        public static final int length_calc_preferences=0x7f040002;
        public static final int preferences=0x7f040003;
    }
}
