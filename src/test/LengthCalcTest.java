package test;

import android.test.AndroidTestCase;
import android.util.Log;

import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-2-13
 * Time: 下午5:27
 * To change this template use File | Settings | File Templates.
 */
public class LengthCalcTest extends AndroidTestCase  {

    private static final String TAG = "LengthCalcTest";

    public void testPattern(){
        String pattern = "^\\d+\\.\\d+$|\\d+";
        Log.v(TAG, Pattern.matches(pattern, "-12.345")+"");
        Log.v(TAG, Pattern.matches(pattern, "12.345")+"");
        Log.v(TAG, Pattern.matches(pattern, "12345")+"");
        Log.v(TAG, Pattern.matches(pattern, "1")+"");
        Log.v(TAG, Pattern.matches(pattern, "1.")+"");
        Log.v(TAG, Pattern.matches(pattern, ".1")+"");
    }
    
    public void testPattern2() {
        assertEquals(true, "123.0".endsWith(".0"));
        assertEquals(true, "123.0".endsWith("\\.0"));
        assertEquals(true, "123.0".endsWith(".0"));
    }


}
