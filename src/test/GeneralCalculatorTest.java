package test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Pattern;

import android.test.AndroidTestCase;
import android.util.Log;

import com.calc.utils.CalcUtils;

public class GeneralCalculatorTest extends AndroidTestCase {
	
	private static final String TAG = "GeneralCalculatorTest";
	
	public void testLoopN() {
		int b = 1;
        int c = (int)5.5;
        while(c!=1) {
            b = c * b;
            c--;
        }
        assertEquals(120, b);
	}

	public void testAdd(){
		double a = 1231.2233123123123123123d, b = 3214.33333333333333333332d;
		BigDecimal b1 = new BigDecimal(Double.toString(a));
		BigDecimal b2 = new BigDecimal(Double.toString(b));
		assertEquals(4445.55664564564564564562,(b1.add(b2)).doubleValue());
	}
	
	public void testAdd2(){
//		double a = 1231.2233123123123123123d, b = 3214.33333333333333333332d;
		double a = 5, b = 3;
//		Log.v(TAG, ""+(a+b));
		assertEquals(8, a+b);
	}

    public void testDivide(){
        double a = 9.09, b = 9;
        Log.v(TAG,  CalcUtils.divide(a, b, 6, RoundingMode.UP));
        assertEquals("1.01", CalcUtils.divide(a, b, 6, RoundingMode.HALF_UP));
    }

    public void testPattern() {
        String str = "2 + 2 = 4 +";
        boolean ret = Pattern.matches(".*\\d$", str);
        assertEquals(true, ret);
    }

    public void testFactorial() {
        int n = 99;
        Log.v(TAG, CalcUtils.getFactorial(n));
        double m = 1;
        while(n>0) {
            m = m * n;
            n--;
        }
        Log.v(TAG, m+"");
    }
    
    public void testSin(){
        double a = 90;
        Log.v(TAG, CalcUtils.sin(a, 1));
    }
    
    public void testGamma() {
        double a[] = {100, 5, 3.5,1, 0,  -0.3, -1, -2, -3, -3.5, -4, -4.3, -4.5, -4.6, -4.7, -4.8, -4.9, -5, -6, -100};
        for(int i=0;i<a.length;i++) {
            Log.v(TAG, a[i]+"="+CalcUtils.gamma(a[i])+"");
        }
    }
}
