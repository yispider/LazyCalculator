package com.calc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.calc.R;
import com.calc.StartActivity;

public class ImageTextGridViewAdapter extends BaseAdapter {

    private Context ctx;
    private int[] items;
    private int[] icons;

    public ImageTextGridViewAdapter(StartActivity startActivity, int[] items, int[] icons) {
        this.ctx = startActivity;
        this.items = items;
        this.icons = icons;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(ctx);
        View view = layoutInflater.inflate(R.layout.image_up_text_down, null);
        ImageView image = (ImageView)view.findViewById(R.id.icon_index);
        image.setImageResource(icons[position]);

        TextView text = (TextView)view.findViewById(R.id.text_index);
//        text.setTextColor(R.color.index_text_color_white);
        text.setText(items[position]);
        return view;
    }
}
