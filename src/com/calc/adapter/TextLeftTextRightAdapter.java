package com.calc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.calc.R;
import com.calc.StartActivity;

public class TextLeftTextRightAdapter extends BaseAdapter {

    private Context ctx;
    private Object[] labels;
    private Object[] contents;

    public TextLeftTextRightAdapter(Context ctx, Object[] labels, Object[] contents) {
        this.ctx = ctx;
        this.labels = labels;
        this.contents = contents;
    }

    @Override
    public int getCount() {
        return labels.length;
    }

    @Override
    public Object getItem(int position) {
        return labels[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(ctx);
        View view = layoutInflater.inflate(R.layout.text_left_text_right, null);

        TextView label = (TextView)view.findViewById(R.id.tt_label);
        label.setText(labels[position].toString());
        TextView content = (TextView)view.findViewById(R.id.tt_content);
        content.setText(contents[position].toString());
        return view;
    }
}
