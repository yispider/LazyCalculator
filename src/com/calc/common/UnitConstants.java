package com.calc.common;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-2-13
 * Time: 上午2:08
 * To change this template use File | Settings | File Templates.
 */
public class UnitConstants {

    /**长度单位**/
    public static final int LENGTH_UNIT_METER = 0;
    public static final int LENGTH_UNIT_KILOMETER = 1;
    public static final int LENGTH_UNIT_DECIMETER = 2;
    public static final int LENGTH_UNIT_CENTIMETER = 3;
    public static final int LENGTH_UNIT_MILLIMETER = 4;
    public static final int LENGTH_UNIT_MICROMETER = 5;
    public static final int LENGTH_UNIT_LI = 6;
    public static final int LENGTH_UNIT_ZHANG = 7;
    public static final int LENGTH_UNIT_CHI = 8;
    public static final int LENGTH_UNIT_CUN = 9;
    public static final int LENGTH_UNIT_FEN = 10;
    public static final int LENGTH_UNIT_LI3= 11;
    public static final int LENGTH_UNIT_HAI_LI = 12;
    public static final int LENGTH_UNIT_YING_XUN = 13;
    public static final int LENGTH_UNIT_YING_LI = 14;
    public static final int LENGTH_UNIT_FU_LONG = 15;
    public static final int LENGTH_UNIT_MA = 16;
    public static final int LENGTH_UNIT_YING_CHI = 17;
    public static final int LENGTH_UNIT_YING_CUN = 18;

    public static final int AREA_UNIT_PING_FANG_MI = 0;
    public static final int AREA_UNIT_PING_FANG_GONG_LI = 1;
    public static final int AREA_UNIT_PING_FANG_FEN_MI = 2;
    public static final int AREA_UNIT_PING_FANG_LI_MI = 3;
    public static final int AREA_UNIT_PING_FANG_HAO_MI = 4;
    public static final int AREA_UNIT_GONG_QING = 5;
    public static final int AREA_UNIT_SHI_MU = 6;
    public static final int AREA_UNIT_YING_MU = 7;
    public static final int AREA_UNIT_PING_FANG_YING_LI = 8;
    public static final int AREA_UNIT_PING_FANG_GAN = 9;
    public static final int AREA_UNIT_PING_FANG_MA = 10;
    public static final int AREA_UNIT_PING_FANG_YING_CHI = 11;
    public static final int AREA_UNIT_PING_FANG_YING_CUN = 12;
}
