package com.calc.common;

import android.view.Menu;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-1-27
 * Time: 下午8:00
 * To change this template use File | Settings | File Templates.
 */
public class Constants {
    
    /**menu group id**/
    public static final int MENU_GROUP_GENERAL_CALCULATOR = 11;
    public static final int MENU_GROUP_LENGTH_CALC = 12;

    //通用计算器菜单
    public static final int MENU_GENERAL_CALCULATOR_CLEAR_FOR_GENERAL_CALCULATOR = Menu.FIRST + 1;
    public static final int MENU_GENERAL_CALCULATOR_SETTING= MENU_GENERAL_CALCULATOR_CLEAR_FOR_GENERAL_CALCULATOR + 1;
    public static final int MENU_GENERAL_CALCULATOR_RETURN_TO_LAST_ACTIVITY = MENU_GENERAL_CALCULATOR_SETTING + 1;
    
    //长度转换器菜单
    public static final int MENU_LENGTH_CALC_SETTING = MENU_GENERAL_CALCULATOR_RETURN_TO_LAST_ACTIVITY + 1;
    public static final int MENU_LENGTH_CALC_RETURN_TO_LAST_ACTIVITY = MENU_LENGTH_CALC_SETTING + 1;
}
