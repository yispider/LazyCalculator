package com.calc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.calc.common.Constants;
import com.calc.common.UnitConstants;
import com.calc.setting.LengthCalcSettingActivity;
import com.calc.utils.LengthUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class LengthCalcActivity extends Activity {

    private static final String TAG = "LengthCalcActivity";

    private Spinner unitSpinner;
    private Button calcButton;
    private ListView outputListView;
    private int unit = UnitConstants.LENGTH_UNIT_METER;
    private SharedPreferences sp;
    private EditText srcLengthEditText;

    private double meter;
    //输如的要转换的长度
    private double srcLength;

    List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.length_calc);
        //禁止横屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        init();
        if(unit != 0) {
            initMeter(unit);
        }
    }

    /**
     * 先将要转换的长度  转换成  米
     * @param unit
     */
    private void initMeter(int unit) {
        switch (unit) {
            case UnitConstants.LENGTH_UNIT_METER:
                meter = srcLength;
            case UnitConstants.LENGTH_UNIT_KILOMETER:
                meter = LengthUtils.kilometer2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_DECIMETER:
                meter = LengthUtils.decimeter2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_CENTIMETER:
                meter = LengthUtils.centimeter2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_MILLIMETER:
                meter = LengthUtils.millimeter2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_MICROMETER:
                meter = LengthUtils.micrometer2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_LI:
                meter = LengthUtils.li2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_ZHANG:
                meter = LengthUtils.zhang2Meter(srcLength) ;
                break;
            case UnitConstants.LENGTH_UNIT_CHI:
                meter = LengthUtils.chi2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_CUN:
                meter = LengthUtils.cun2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_FEN:
                meter = LengthUtils.fen2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_LI3:
                meter = LengthUtils.li32Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_HAI_LI:
                meter = LengthUtils.haili2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_YING_XUN:
                meter = LengthUtils.yingxun2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_YING_LI:
                meter = LengthUtils.yingli2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_FU_LONG:
                meter = LengthUtils.fuLong2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_MA:
                meter = LengthUtils.ma2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_YING_CHI:
                meter = LengthUtils.yingchi2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_YING_CUN:
                meter = LengthUtils.yingcun2Meter(srcLength);
                break;
            default:
                meter = srcLength;
                break;
        }
    }

    private void init() {
        //取出默认单位
        String defaultUnit = sp.getString("length_calc_src_unit", String.valueOf(UnitConstants.LENGTH_UNIT_METER));
        Log.d(TAG, "defaultUnit="+defaultUnit);
        unit = Integer.parseInt(defaultUnit);
        //单位下拉框初始化
        unitSpinner = (Spinner)findViewById(R.id.unit);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.app_length_calc_unit_chose, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unitSpinner.setAdapter(adapter);
        unitSpinner.setPromptId(R.string.length_calc_unit_spinner_prompt);
        unitSpinner.setSelection(unit);
        unitSpinner.setOnItemSelectedListener(unitSelected);

        srcLengthEditText = (EditText)findViewById(R.id.src_length);

        calcButton = (Button)findViewById(R.id.calc_btn);
        calcButton.setOnClickListener(doCalc);

        outputListView = (ListView)findViewById(R.id.length_calc_output_area);
    }
    
    private Spinner.OnItemSelectedListener unitSelected = new Spinner.OnItemSelectedListener(){
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            //UnitConstants中单位的顺序与Spinner中相同，所以可以直接将position值赋给unit
            unit = position;
        }
        public void onNothingSelected(AdapterView<?> parent) {
            //do nothing
        }
    };
    
    private Button.OnClickListener doCalc = new Button.OnClickListener(){
        public void onClick(View v) {
            //校验输入内容
            String srcLengthStr = srcLengthEditText.getText().toString();
            if(srcLengthStr.length()==0) {
                new AlertDialog.Builder(LengthCalcActivity.this)
                        .setTitle(R.string.input_empty)
                        .setMessage(R.string.input_empty)
                        .setNegativeButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
            } else if(!Pattern.matches("^\\d+\\.\\d+$|\\d+", srcLengthStr)) {
                new AlertDialog.Builder(LengthCalcActivity.this)
                        .setTitle(R.string.input_error)
                        .setMessage(R.string.input_error)
                        .setNegativeButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
            } else {
                srcLength = Double.valueOf(srcLengthStr);
                data = new ArrayList<Map<String, Object>>();
                Map<String, Object> map = new HashMap<String, Object>();
                if(sp.getBoolean("length_calc_meter_preference", true) && unit!=UnitConstants.LENGTH_UNIT_METER) {
                    map.put("label", getResources().getString(R.string.length_calc_meter_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    map.put("content", meterValue);
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_kilometer_preference", true) && unit != UnitConstants.LENGTH_UNIT_KILOMETER) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_kilometer_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Kilometer(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_decimeter_preference", true) && unit != UnitConstants.LENGTH_UNIT_DECIMETER) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_decimeter_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Decimeter(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_centimeter_preference", true) && unit != UnitConstants.LENGTH_UNIT_CENTIMETER) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_centimeter_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Centimeter(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_millimeter_preference", true) && unit != UnitConstants.LENGTH_UNIT_MILLIMETER) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_millimeter_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Millimeter(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_micrometer_preference", true) && unit != UnitConstants.LENGTH_UNIT_MICROMETER) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_micrometer_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Micrometer(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_li_preference", true) && unit != UnitConstants.LENGTH_UNIT_LI) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_li_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Li(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_zhang_preference", true) && unit != UnitConstants.LENGTH_UNIT_ZHANG) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_zhang_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Zhang(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_chi_preference", true) && unit != UnitConstants.LENGTH_UNIT_CHI) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_chi_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Chi(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_cun_preference", true) && unit != UnitConstants.LENGTH_UNIT_CUN) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_cun_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Cun(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_fen_preference", true) && unit != UnitConstants.LENGTH_UNIT_FEN) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_fen_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Fen(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_li3_preference", true) && unit != UnitConstants.LENGTH_UNIT_LI3) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_li3_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Li3(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_hai_li_preference", true) && unit != UnitConstants.LENGTH_UNIT_HAI_LI) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_hai_li_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2HaiLi(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_ying_xun_preference", true) && unit != UnitConstants.LENGTH_UNIT_YING_XUN) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_ying_xun_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2YingXun(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_ying_li_preference", true) && unit != UnitConstants.LENGTH_UNIT_YING_LI) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_ying_li_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2YingLi(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_ying_li_preference", true) && unit != UnitConstants.LENGTH_UNIT_MA) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_ma_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2Ma(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_fu_long_preference", true) && unit != UnitConstants.LENGTH_UNIT_FU_LONG) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_fu_long_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2FuLong(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_ying_chi_preference", true) && unit != UnitConstants.LENGTH_UNIT_YING_CHI) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_ying_chi_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2YingChi(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                if(sp.getBoolean("length_calc_ying_cun_preference", true) && unit != UnitConstants.LENGTH_UNIT_YING_CUN) {
                    map = new HashMap<String, Object>();
                    map.put("label", getResources().getString(R.string.length_calc_ying_cun_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    double realValue = LengthUtils.meter2YingCun(meterValue);
                    map.put("content", LengthUtils.format(realValue));
                    data.add(map);
                }
                SimpleAdapter adapter = new SimpleAdapter(LengthCalcActivity.this, data, R.layout.text_left_text_right,
                        new String[]{"label", "content"}, new int[]{R.id.tt_label, R.id.tt_content});
                outputListView.setAdapter(adapter);
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //group id, menu id, order , name
        menu.add(Constants.MENU_GROUP_LENGTH_CALC, Constants.MENU_LENGTH_CALC_SETTING, 1, R.string.length_calc_menu_setting).setIcon(R.drawable.setting);
        menu.add(Constants.MENU_GROUP_LENGTH_CALC, Constants.MENU_LENGTH_CALC_RETURN_TO_LAST_ACTIVITY, 2, R.string.return_last_activity).setIcon(R.drawable.back);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case Constants.MENU_LENGTH_CALC_SETTING:
                Intent intent = new Intent(LengthCalcActivity.this, LengthCalcSettingActivity.class);
                startActivity(intent);
                break;
            case Constants.MENU_LENGTH_CALC_RETURN_TO_LAST_ACTIVITY :
                this.finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public static double srcLengthToMeter(int u, double v) {
        switch (u) {
            case UnitConstants.LENGTH_UNIT_METER :
                return v;
            case UnitConstants.LENGTH_UNIT_KILOMETER:
                return LengthUtils.kilometer2Meter(v);
            case UnitConstants.LENGTH_UNIT_DECIMETER:
                return LengthUtils.decimeter2Meter(v);
            case UnitConstants.LENGTH_UNIT_CENTIMETER:
                return LengthUtils.centimeter2Meter(v);
            case UnitConstants.LENGTH_UNIT_MILLIMETER:
                return LengthUtils.millimeter2Meter(v);
            case UnitConstants.LENGTH_UNIT_MICROMETER:
                return LengthUtils.micrometer2Meter(v);
            case UnitConstants.LENGTH_UNIT_LI:
                return LengthUtils.li2Meter(v);
            case UnitConstants.LENGTH_UNIT_ZHANG:
                return LengthUtils.zhang2Meter(v);
            case UnitConstants.LENGTH_UNIT_CHI:
                return LengthUtils.chi2Meter(v);
            case UnitConstants.LENGTH_UNIT_CUN:
                return LengthUtils.cun2Meter(v);
            case UnitConstants.LENGTH_UNIT_FEN:
                return LengthUtils.fen2Meter(v);
            case UnitConstants.LENGTH_UNIT_LI3:
                return LengthUtils.li32Meter(v);
            case UnitConstants.LENGTH_UNIT_HAI_LI:
                return LengthUtils.haili2Meter(v);
            case UnitConstants.LENGTH_UNIT_YING_XUN:
                return LengthUtils.yingxun2Meter(v);
            case UnitConstants.LENGTH_UNIT_YING_LI:
                return LengthUtils.yingli2Meter(v);
            case UnitConstants.LENGTH_UNIT_FU_LONG:
                return LengthUtils.fuLong2Meter(v);
            case UnitConstants.LENGTH_UNIT_MA:
                return LengthUtils.ma2Meter(v);
            case UnitConstants.LENGTH_UNIT_YING_CHI:
                return LengthUtils.yingchi2Meter(v);
            case UnitConstants.LENGTH_UNIT_YING_CUN:
                return LengthUtils.yingxun2Meter(v);
            default:
                return v;
        }
    }
}