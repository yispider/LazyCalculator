package com.calc.utils;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-2-3
 * Time: 上午11:43
 * To change this template use File | Settings | File Templates.
 */
public class Toolkit {


    public static String readTextFromAssets(Context ctx, String fileName) {
        AssetManager assetManager = ctx.getAssets();
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(assetManager.openFd(fileName).getFileDescriptor()));
            while(reader.readLine() != null) {
                sb.append(reader.readLine() + "\n");
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
