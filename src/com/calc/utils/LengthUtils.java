package com.calc.utils;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-2-8
 * Time: 下午11:03
 * To change this template use File | Settings | File Templates.
 */
public class LengthUtils {

    //米--千米    1米=0.001千米    1千米=1 000米
    public static final double METER_KILOMETER = 0.001;
    //米--分米    1米=10分米       1分米=0.1米
    public static final int METER_DECIMETER = 10;
    //米--厘米    1米=100厘米       1厘米=0.01米
    public static final int METER_CENTIMETER = 100;
    //米--毫米    1米=1 000毫米     1毫米=0.001米
    public static final int METER_MILLIMETER = 1000;
    //米--微米   1米=1 000 000微米  1微米=1*10-6米
    public static final int METER_MICROMETER = 1000000;
    //米--里        1米=0.002里     1里=500米
    public static final float METER_LI = 0.002f;
    //米--丈        1米=0.3丈       1丈=3.3333333333333米
    public static final float METER_ZHANG = 0.3f;
    //米--尺        1米=3尺        1尺=0.33333333333333米
    public static final int METER_CHI = 3;
    //米--寸        1米=30寸       1寸=0.033333333333333米
    public static final int METER_CUN = 30;
    //米--分        1米=300分     1分=0.0033333333333333米
    public static final int METER_FEN = 300;
    //米--厘        1米=3 000厘   1厘=0.00033333333333333米
    public static final int METER_LI3 = 3000;
    //米--海里(nmi) 1米=0.00053995680345572海里  1海里=1 852米
    public static final double METER_HAI_LI = 0.00053995680345572d;
    //米--英寻      1米=0.54680664916885英寻  1英寻=1.8288米
    public static final double METER_YING_XUN = 0.3f;
    //米--英里(mi)  1米=0.00062137119223733英里  1英里=1 609.344米
    public static final double METER_YING_LI = 0.00062137119223733d;
    //米--弗隆(fur) 1米=0.0049709695378987弗隆  1弗隆=201.168米
    public static final double METER_FU_LONG = 0.0049709695378987d;
    //米--码(yd)    1米=1.0936132983377码   1码=0.9144米
    public static final double METER_MA = 1.0936132983377d;
    //米--英尺(ft)  1米=3.2808398950131英尺  1英尺=0.3048米
    public static final double METER_YING_CHI = 3.2808398950131d;
    //米--英寸(in)  1米=39.370078740157英寸  1英寸=0.0254米
    public static final double METER_YING_CUN = 39.370078740157d;



    public static String format(double aa) {
        String ss = String.valueOf(aa);
        if(ss.endsWith("\u002e0")) {
            ss = ss.substring(0, ss.length()-2);
        }
        return ss;
    }

    /**
     * 米  转   公里
     * @param meter
     * @return
     */
    public static double meter2Kilometer(double meter) {
        return meter * METER_KILOMETER;
    }
    /**
     *  公里 转  米
     * @param kilometer
     * @return
     */
    public static double kilometer2Meter(double kilometer) {
        return kilometer / METER_KILOMETER;
    }

    /**
     * 米  转   分米
     * @param meter
     * @return
     */
    public static double meter2Decimeter(double meter) {
        return meter * METER_DECIMETER;
    }

    public static double decimeter2Meter(double dm) {
        return dm / METER_DECIMETER;
    }

    /**
     * 米  转   厘米
     * @param meter
     * @return
     */
    public static double meter2Centimeter(double meter) {
        return meter * METER_CENTIMETER;
    }

    public static double centimeter2Meter(double cm) {
        return cm / METER_CENTIMETER;
    }
    /**
     * 米  转   毫米
     * @param meter
     * @return
     */
    public static double meter2Millimeter(double meter) {
        return meter * METER_MILLIMETER;
    }
    public static double millimeter2Meter(double meter) {
        return meter / METER_MILLIMETER ;
    }

    /**
     * 米  转   微米
     * @param meter
     * @return
     */
    public static double meter2Micrometer(double meter) {
        return meter * METER_MICROMETER;
    }

    public static double micrometer2Meter(double mm) {
        return mm / METER_MICROMETER;
    }
    /**
     * 米  转   里
     * @param meter
     * @return
     */
    public static double meter2Li(double meter) {
        return meter * METER_LI;
    }
    public static double li2Meter(double li) {
        return li / METER_LI;
    }
    /**
     * 米  转  丈
     * @param meter
     * @return
     */
    public static double meter2Zhang(double meter) {
        return meter * METER_ZHANG;
    }
    public static double zhang2Meter(double zhang) {
        return zhang / METER_ZHANG;
    }
    /**
     * 米  转  尺
     * @param meter
     * @return
     */
    public static double meter2Chi(double meter) {
        return meter * METER_CHI;
    }
    public static double chi2Meter(double chi) {
        return chi / METER_CHI;
    }
    /**
     * 米  转  寸
     * @param meter
     * @return
     */
    public static double meter2Cun(double meter) {
        return meter * METER_CUN;
    }
    public static double cun2Meter(double cun) {
        return cun / METER_CUN;
    }
    /**
     * 米  转  分
     * @param meter
     * @return
     */
    public static double meter2Fen(double meter) {
        return meter * METER_FEN;
    }
    public static double fen2Meter(double fen) {
        return fen / METER_FEN;
    }
    /**
     * 米  转 厘
     * @param meter
     * @return
     */
    public static double meter2Li3(double meter) {
        return meter * METER_LI3;
    }
    public static double li32Meter(double li3) {
        return li3 / METER_LI3;
    }
    /**
     * 米  转  海里
     * @param meter
     * @return
     */
    public static double meter2HaiLi(double meter) {
        return meter * METER_HAI_LI;
    }
    public static double haili2Meter(double haili) {
        return haili / METER_HAI_LI;
    }
    /**
     * 米  转  英寻
     * @param meter
     * @return
     */
    public static double meter2YingXun(double meter) {
        return meter * METER_YING_XUN;
    }
    public static double yingxun2Meter(double yingxun) {
        return yingxun / METER_YING_XUN;
    }
    /**
     * 米  转  英里
     * @param meter
     * @return
     */
    public static double meter2YingLi(double meter) {
        return meter * METER_YING_LI;
    }
    public static double yingli2Meter(double yingli) {
        return yingli / METER_YING_LI;
    }
    /**
     * 米  转  弗隆
     * @param meter
     * @return
     */
    public static double meter2FuLong(double meter) {
        return meter * METER_FU_LONG;
    }
    public static double fuLong2Meter(double fuLong) {
        return fuLong / METER_FU_LONG;
    }
    /**
     * 米  转  码
     * @param meter
     * @return
     */
    public static double meter2Ma(double meter) {
        return meter * METER_MA;
    }
    public static double ma2Meter(double ma) {
        return ma / METER_MA;
    }
    /**
     * 米  转   英尺
     * @param meter
     * @return
     */
    public static double meter2YingChi(double meter) {
        return meter * METER_YING_CHI;
    }
    public static double yingchi2Meter(double yingchi) {
        return yingchi / METER_YING_CHI;
    }
    /**
     * 米  转  英寸
     * @param meter
     * @return
     */
    public static double meter2YingCun(double meter) {
        return meter * METER_YING_CUN;
    }
    public static double yingcun2Meter(double yingcun) {
        return yingcun / METER_YING_CUN;
    }
}
