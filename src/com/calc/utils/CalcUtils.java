package com.calc.utils;

import android.util.Log;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;


public class CalcUtils {
	
	public static String add(double a, double b){
		return String.valueOf(a+b);
	}
	
	public static String minus(double subtractor, double subtrahend){
		return String.valueOf(subtractor - subtrahend);
	}
	
	
	public static String multiply(double multiplier, double multiplicand){
		return String.valueOf(multiplier * multiplicand);
	}

    /**
     * 通过BigInteger来获取较为精确的阶乘值
     * @param n
     * @return
     */
	public static String getExactFactorial(int n) {
		// 将数组换成字符串后构造BigInteger
		BigInteger result = new BigInteger("1");
		for (; n > 0; n--) {
			// 将数字n转换成字符串后，再构造一个BigInteger对象，与现有结果做乘法
			result = result.multiply(new BigInteger(new Integer(n).toString()));
		}
		return new BigDecimal(result).toString();
	}

    /**
     * 使用普通double数计算阶乘，获取以科学计数法表示的结果
     * @param n
     * @return
     */
    public static String getFactorial(int n) {
        // 将数组换成字符串后构造BigInteger
        BigInteger result = new BigInteger("1");
        for (; n > 0; n--) {
            // 将数字n转换成字符串后，再构造一个BigInteger对象，与现有结果做乘法
            result = result.multiply(new BigInteger(new Integer(n).toString()));
        }
        return new BigDecimal(result).toString();
    }

    /**
     * 被除数÷除数
     * @param dividend  被除数
     * @param divisor 除数
     * @param scale 小数位数
     * @param roundingMode 输入模式 default:HALF_UP,即四舍五入
     * @return
     */
    public static String divide(double dividend, double divisor, int scale, RoundingMode roundingMode) {
        if(roundingMode == null ) {
            roundingMode = RoundingMode.HALF_UP;
        }
        String result = new BigDecimal(dividend).divide(new BigDecimal(divisor), scale, roundingMode).toString();
        while(result.indexOf(".")>0 && result.endsWith("0")) {
            result = result.substring(0, result.length()-1);
        }
        if(result.endsWith(".")) {
            result = result.substring(0, result.length()-1);
        }
        return result;
    }
	
    /**
     * a的b次方
     * @param a
     * @param b
     * @return
     */
    public static String pow(double a, double b) {
    	return Math.pow(a, b)+"";
    }
    /**
     * a的1/b次方
     * @param a
     * @param b
     * @return
     */
    public static String powRoot(double a, double b){
    	return Math.pow(a, 1/b)+"";
    }

    public static String sin(double a, int flag) {
        String result = "";
        if(flag == 1) {
            //数字转换成度数
            a = num2Degree(a);
        }
        result = String.valueOf(Math.sin(a));
        return result;
    }
    public static String cos(double a, int flag) {
        String result = "";
        if(flag == 1) {
            //数字转换成度数
            a = num2Degree(a);
        }
        result = String.valueOf(Math.cos(a));
        return result;
    }
    public static String tan(double a, int flag) {
        String result = "";
        if(flag == 1) {
            //数字转换成度数
            a = num2Degree(a);
        }
        result = String.valueOf(Math.tan(a));
        return result;
    }

    /**
     * 数字转换成角度值
     * @param num
     * @return
     */
    public static double num2Degree(double num) {
        return num * Math.PI / 180;
    }

    /**
     * 伽马函数，用于计算小数阶乘
     * @param x
     * @return
     */
    public static double gamma(double x) {
        double ret = 0.0;
        ret = (1.000000000190015
                + 76.18009172947146/(x+2)
                - 86.50532032941677/(x+3)
                + 24.01409824083091/(x+4)
                - 1.231739572450155/(x+5)
                + 0.001208650973866179/(x+6)
                - 0.000005395239384953/(x+7) )
                * (Math.sqrt(2 * Math.PI)/(x+1))
                * Math.pow((x+6.5), (x+1.5))
                * Math.pow(Math.E, (-(x+6.5)));
        return ret;
    }
}
