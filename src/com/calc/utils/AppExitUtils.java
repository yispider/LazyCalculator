package com.calc.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-2-3
 * Time: 上午10:25
 * To change this template use File | Settings | File Templates.
 */
public class AppExitUtils {

    private static List<Activity> activityList = new ArrayList<Activity>();

    private static Activity currentActivity;

    /**
     * 将此Activity加入到活动Activity队列中
     * @param activity
     * @return
     */
    public static boolean addActivity(Activity activity) {
        boolean flag = activityList.add(activity);
        //Log.i(LOG_TAG, "addActivity " + activity.getClass().getSimpleName() + activityList.size());
        return flag;
    }

    /**
     * 从活动Activity队列中移除该Activity
     * @param activity
     * @return
     */
    public static boolean removeActivity(Activity activity) {
        boolean flag = activityList.remove(activity);
        //Log.i(LOG_TAG, "removeActivity " + activity.getClass().getSimpleName() + activityList.size());
        return flag;
    }

    /**
     * 获得所有未关闭的活动（Activity）。
     * @return
     */
    public static List<Activity> getActivities() {
        /*
        * 拷贝一个List对象作为返回结果，一方面为了防止外部修改内部List数据，
        * 另一方面防止内部的修改影响到外部调用。
        */
        List<Activity> copyActivityList = new ArrayList<Activity>(activityList);
        return copyActivityList;
    }

    /**
     * 关闭程序。
     */
    public static void exit(ActivityManager activityMgr, String packageName) {
        List<Activity> activities = getActivities();
        for (Activity activity : activities) {
            if (activity != null) {
                activity.finish();
            }
        }
        int version = android.os.Build.VERSION.SDK_INT;
        if (version <= 7) {
            activityMgr.restartPackage(packageName);
        } else {
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    /**
     * 设置当前Activity实例
     * @param activity
     * @return
     */
    public static void setCurrentActivity(Activity activity) {
        currentActivity = activity;
    }

    /**
     * 获得当前显示的Activity。
     * @return
     */
    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    /**
     * 退出程序时弹出提示窗口
     */
    public static void showExitTips(final Context ctx){
        AlertDialog alertDialog = new AlertDialog.Builder(ctx)
                .setTitle("提示")
                .setMessage("是否退出本程序？")
                .setPositiveButton("确定",new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exit((ActivityManager)ctx.getSystemService(Context.ACTIVITY_SERVICE),ctx.getPackageName());
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                }).create();
        alertDialog.show();
    }

}
