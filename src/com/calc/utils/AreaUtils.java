package com.calc.utils;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-2-18
 * Time: 上午12:29
 * To change this template use File | Settings | File Templates.
 */
public class AreaUtils {

    //p平方米
    public static final double PING_FANG_MI = 1;
    //平方公里  -->  平方米
    public static final double PING_FANG_GONG_LI_2_PING_FANG_MI = 1000000;
    //平方分米  -->  平方米
    public static final double PING_FANG_FEN_MI_2_PING_FANG_MI = 0.01;
    //平方厘米  -->  平方米
    public static final double PING_FANG_LI_MI_2_PING_FANG_MI = 0.0001;
    //平方毫米  -->  平方米
    public static final double PING_FANG_HAO_MI_2_PING_FANG_MI = 0.000001;
    //公顷  -->  平方米
    public static final double GONG_QING_2_PING_FANG_MI = 10000;
    //亩  -->  平方米
    public static final double SHI_MU_2_PING_FANG_MI = 666.66666666667;
    //英亩  -->  平方米
    public static final double YING_MU_2_PING_FANG_MI = 4046.8564224;
    //平方英里  -->  平方米
    public static final double PING_FANG_YING_LI_2_PING_FANG_MI = 2589988.110336;
    //平方竿  -->  平方米
    public static final double PING_FANG_GAN_2_PING_FANG_MI = 25.29285264;
    //平方码  -->  平方米
    public static final double PING_FANG_MA_2_PING_FANG_MI = 0.83612736;
    //平方英尺  -->  平方米
    public static final double PING_FANG_YING_CHI_2_PING_FANG_MI = 0.09290304;
    //平方英寸  -->  平方米
    public static final double PING_FANG_YING_CUN_2_PING_FANG_MI = 0.00064516;

    //平方公里  -->  平方米
    public static double pfgl2Pfm(double s) {
        return s * PING_FANG_GONG_LI_2_PING_FANG_MI;
    }
    public static double pfm2Pfgl(double s) {
        return s / PING_FANG_GONG_LI_2_PING_FANG_MI;
    }
    //平方分米  -->  平方米
    public static double pffm2Pfm(double s) {
        return s * PING_FANG_FEN_MI_2_PING_FANG_MI;
    }
    public static double pfm2Pffm(double s) {
        return s / PING_FANG_FEN_MI_2_PING_FANG_MI;
    }

    //平方厘米  -->  平方米
    public static double pflm2Pfm(double s) {
        return s * PING_FANG_LI_MI_2_PING_FANG_MI;
    }
    public static double pfm2Pflm(double s){
        return s / PING_FANG_LI_MI_2_PING_FANG_MI;
    }
    //平方毫米  -->  平方米
    public static double pfhm2Pfm(double s) {
        return s * PING_FANG_HAO_MI_2_PING_FANG_MI;
    }
    public static double pfm2Pfhm(double pfm){
        return pfm / PING_FANG_HAO_MI_2_PING_FANG_MI;
    }
    //公顷  -->  平方米
    public static double gongQing2Pfm(double s) {
        return s * GONG_QING_2_PING_FANG_MI;
    }
    public static double pfm2GongQing(double s){
        return s / GONG_QING_2_PING_FANG_MI;
    }
    //亩  -->  平方米
    public static double shiMu2Pfm(double s) {
        return s * SHI_MU_2_PING_FANG_MI;
    }
    public static double pfm2ShiMu(double s){
        return s / SHI_MU_2_PING_FANG_MI;
    }
    //英亩  -->  平方米
    public static double yingMu2Pfm(double s) {
        return s * YING_MU_2_PING_FANG_MI;
    }
    public static double pfm2YingMu(double s){
        return s / YING_MU_2_PING_FANG_MI;
    }
    //平方英里  -->  平方米
    public static double pfyl2Pfm(double s) {
        return s * PING_FANG_YING_LI_2_PING_FANG_MI;
    }
    public static double pfm2Pfyl(double s){
        return s / PING_FANG_YING_LI_2_PING_FANG_MI;
    }
    //平方竿  -->  平方米
    public static double pfg2Pfm(double s) {
        return s * PING_FANG_GAN_2_PING_FANG_MI;
    }
    public static double pfm2Pfg(double s){
        return s / PING_FANG_GAN_2_PING_FANG_MI;
    }
    //平方码  -->  平方米
    public static double pfma2Pfm(double s) {
        return s * PING_FANG_MA_2_PING_FANG_MI;
    }
    public static double pfm2Pfma(double s){
        return s / PING_FANG_MA_2_PING_FANG_MI;
    }
    //平方英尺  -->  平方米
    public static double pfyc2Pfm(double s) {
        return s * PING_FANG_YING_CHI_2_PING_FANG_MI;
    }
    public static double pfm2Pfyc(double s){
        return s / PING_FANG_YING_CHI_2_PING_FANG_MI;
    }
    //平方英寸  -->  平方米
    public static double pfycun2Pfm(double s) {
        return s * PING_FANG_YING_CUN_2_PING_FANG_MI;
    }
    public static double pfm2Pfycun(double s){
        return s / PING_FANG_YING_CUN_2_PING_FANG_MI;
    }
}
