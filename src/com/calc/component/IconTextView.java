package com.calc.component;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-2-9
 * Time: 上午11:05
 * To change this template use File | Settings | File Templates.
 */
public class IconTextView extends TextView {

    //  命名空间的值
    private final String namespace = "http://com.calc.qu";
    //  保存图像资源ID的变量
    private int resourceId = 0;
    private Bitmap bitmap;

    public IconTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        resourceId = attrs.getAttributeResourceValue(namespace, "icon_src", 0);
        if (resourceId > 0)
            //  如果成功获得图像资源的ID，装载这个图像资源，并创建Bitmap对象
            bitmap = BitmapFactory.decodeResource(getResources(), resourceId);
    }

    protected void onDraw(Canvas canvas) {
        if (bitmap != null) {
            /*//要从原图上截取图像的区域
            Rect src = new Rect();
            src.left = 0;
            src.top = 0;
            src.right = bitmap.getWidth();
            src.bottom = bitmap.getHeight();
            int textSize = (int) getTextSize();
            //将截取的图像复制到bitmap上的目标区域
            Rect target = new Rect();
            target.left = 0;
            //计算图像复制到目标区域的纵坐标。由于TextView组件的文本内容并不是从最顶端开始绘制的，因此，需要重新计算绘制图像的纵坐标
            target.top = (int) ((getMeasuredHeight() - textSize) / 2) + 1;
            target.bottom = target.top + textSize;
            //  为了保证图像不变形，需要根据图像高度重新计算图像的宽度
            target.right = (int) (textSize * (bitmap.getWidth() / (float) bitmap.getHeight()));
            //  开始绘制图像
            canvas.drawBitmap(bitmap, src, target, getPaint());
            //  将TextView中的文本向右移动一定的距离（在本例中移动了图像宽度加2个象素点的位置）
            canvas.translate(target.right + 2, 0);*/

            //要从原图上截取图像的区域
            Rect src = new Rect();
            src.left = 0;
            src.top = 0;
            src.right = getWidth();
            src.bottom = getHeight();
            int textSize = (int) getTextSize();
            //将截取的图像复制到bitmap上的目标区域
            Rect target = new Rect();
            target.left = 0;
            //计算图像复制到目标区域的纵坐标。由于TextView组件的文本内容并不是从最顶端开始绘制的，因此，需要重新计算绘制图像的纵坐标
            target.top = (int) ((getMeasuredHeight() - textSize) / 2) + 1;
            target.bottom = target.top + textSize;
            //  为了保证图像不变形，需要根据图像高度重新计算图像的宽度
            target.right = (int) (textSize * (bitmap.getWidth() / (float) bitmap.getHeight()));
            //  开始绘制图像
            canvas.drawBitmap(bitmap, src, target, getPaint());
            //  将TextView中的文本向右移动一定的距离（在本例中移动了图像宽度加2个象素点的位置）
            canvas.translate(target.right + 2, 0);
        }
        super.onDraw(canvas);
    }
}