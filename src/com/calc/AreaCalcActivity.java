package com.calc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.calc.common.UnitConstants;
import com.calc.utils.LengthUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-3-4
 * Time: 下午5:59
 * To change this template use File | Settings | File Templates.
 */
public class AreaCalcActivity extends Activity {

    private static final String TAG = "AreaCalcActivity";

    private Spinner unitSpinner;
    private Button calcButton;
    private ListView outputListView;
    private int unit = UnitConstants.AREA_UNIT_PING_FANG_MI;
    private SharedPreferences sp;
    private EditText srcLengthEditText;

    private double pingFangMi;
    //输如的要转换的长度
    private double srcLength;

    private double meter;

    List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.length_calc);
        //禁止横屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        init();
        if(unit != 0) {
            initUnit(unit);
        }
    }

    private void init() {
        //取出默认单位
        String defaultUnit = sp.getString("area_calc_src_unit", String.valueOf(UnitConstants.LENGTH_UNIT_METER));
        Log.d(TAG, "defaultUnit=" + defaultUnit);
        unit = Integer.parseInt(defaultUnit);
        //单位下拉框初始化
        unitSpinner = (Spinner)findViewById(R.id.unit);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.app_length_calc_unit_chose, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unitSpinner.setAdapter(adapter);
        unitSpinner.setPromptId(R.string.length_calc_unit_spinner_prompt);
        unitSpinner.setSelection(unit);
        unitSpinner.setOnItemSelectedListener(unitSelected);

        srcLengthEditText = (EditText)findViewById(R.id.src_length);

        calcButton = (Button)findViewById(R.id.calc_btn);
        calcButton.setOnClickListener(doCalc);

        outputListView = (ListView)findViewById(R.id.length_calc_output_area);
    }

    private Spinner.OnItemSelectedListener unitSelected = new Spinner.OnItemSelectedListener(){
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            //UnitConstants中单位的顺序与Spinner中相同，所以可以直接将position值赋给unit
            unit = position;
        }
        public void onNothingSelected(AdapterView<?> parent) {
            //do nothing
        }
    };

    /**
     * 先将要转换的长度  转换成  米
     * @param unit
     */
    private void initUnit(int unit) {
        switch (unit) {
            case UnitConstants.LENGTH_UNIT_METER:
                meter = srcLength;
            case UnitConstants.LENGTH_UNIT_KILOMETER:
                meter = LengthUtils.kilometer2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_DECIMETER:
                meter = LengthUtils.decimeter2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_CENTIMETER:
                meter = LengthUtils.centimeter2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_MILLIMETER:
                meter = LengthUtils.millimeter2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_MICROMETER:
                meter = LengthUtils.micrometer2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_LI:
                meter = LengthUtils.li2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_ZHANG:
                meter = LengthUtils.zhang2Meter(srcLength) ;
                break;
            case UnitConstants.LENGTH_UNIT_CHI:
                meter = LengthUtils.chi2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_CUN:
                meter = LengthUtils.cun2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_FEN:
                meter = LengthUtils.fen2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_LI3:
                meter = LengthUtils.li32Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_HAI_LI:
                meter = LengthUtils.haili2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_YING_XUN:
                meter = LengthUtils.yingxun2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_YING_LI:
                meter = LengthUtils.yingli2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_FU_LONG:
                meter = LengthUtils.fuLong2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_MA:
                meter = LengthUtils.ma2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_YING_CHI:
                meter = LengthUtils.yingchi2Meter(srcLength);
                break;
            case UnitConstants.LENGTH_UNIT_YING_CUN:
                meter = LengthUtils.yingcun2Meter(srcLength);
                break;
            default:
                meter = srcLength;
                break;
        }
    }

    private Button.OnClickListener doCalc = new Button.OnClickListener(){
        public void onClick(View v) {
            //校验输入内容
            String srcLengthStr = srcLengthEditText.getText().toString();
            if(srcLengthStr.length()==0) {
                new AlertDialog.Builder(AreaCalcActivity.this)
                        .setTitle(R.string.input_empty)
                        .setMessage(R.string.input_empty)
                        .setNegativeButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
            } else if(!Pattern.matches("^\\d+\\.\\d+$|\\d+", srcLengthStr)) {
                new AlertDialog.Builder(AreaCalcActivity.this)
                        .setTitle(R.string.input_error)
                        .setMessage(R.string.input_error)
                        .setNegativeButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
            } else {
                srcLength = Double.valueOf(srcLengthStr);
                data = new ArrayList<Map<String, Object>>();
                Map<String, Object> map = new HashMap<String, Object>();
                if(sp.getBoolean("length_calc_meter_preference", true) && unit!=UnitConstants.LENGTH_UNIT_METER) {
                    map.put("label", getResources().getString(R.string.length_calc_meter_name));
                    double meterValue = srcLengthToMeter(unit, srcLength);
                    map.put("content", meterValue);
                    data.add(map);
                }
                SimpleAdapter adapter = new SimpleAdapter(AreaCalcActivity.this, data, R.layout.text_left_text_right,
                        new String[]{"label", "content"}, new int[]{R.id.tt_label, R.id.tt_content});
                outputListView.setAdapter(adapter);
            }
        }
    };

    public static double srcLengthToMeter(int u, double v) {
        switch (u) {
            case UnitConstants.LENGTH_UNIT_METER :
                return v;
            case UnitConstants.LENGTH_UNIT_KILOMETER:
                return LengthUtils.kilometer2Meter(v);
            case UnitConstants.LENGTH_UNIT_DECIMETER:
                return LengthUtils.decimeter2Meter(v);
            case UnitConstants.LENGTH_UNIT_CENTIMETER:
                return LengthUtils.centimeter2Meter(v);
            case UnitConstants.LENGTH_UNIT_MILLIMETER:
                return LengthUtils.millimeter2Meter(v);
            case UnitConstants.LENGTH_UNIT_MICROMETER:
                return LengthUtils.micrometer2Meter(v);
            case UnitConstants.LENGTH_UNIT_LI:
                return LengthUtils.li2Meter(v);
            case UnitConstants.LENGTH_UNIT_ZHANG:
                return LengthUtils.zhang2Meter(v);
            case UnitConstants.LENGTH_UNIT_CHI:
                return LengthUtils.chi2Meter(v);
            case UnitConstants.LENGTH_UNIT_CUN:
                return LengthUtils.cun2Meter(v);
            case UnitConstants.LENGTH_UNIT_FEN:
                return LengthUtils.fen2Meter(v);
            case UnitConstants.LENGTH_UNIT_LI3:
                return LengthUtils.li32Meter(v);
            case UnitConstants.LENGTH_UNIT_HAI_LI:
                return LengthUtils.haili2Meter(v);
            case UnitConstants.LENGTH_UNIT_YING_XUN:
                return LengthUtils.yingxun2Meter(v);
            case UnitConstants.LENGTH_UNIT_YING_LI:
                return LengthUtils.yingli2Meter(v);
            case UnitConstants.LENGTH_UNIT_FU_LONG:
                return LengthUtils.fuLong2Meter(v);
            case UnitConstants.LENGTH_UNIT_MA:
                return LengthUtils.ma2Meter(v);
            case UnitConstants.LENGTH_UNIT_YING_CHI:
                return LengthUtils.yingchi2Meter(v);
            case UnitConstants.LENGTH_UNIT_YING_CUN:
                return LengthUtils.yingxun2Meter(v);
            default:
                return v;
        }
    }
}