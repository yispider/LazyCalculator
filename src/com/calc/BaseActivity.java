package com.calc;

import android.app.Activity;
import android.os.Bundle;
import com.calc.utils.AppExitUtils;

public class BaseActivity extends Activity{

    public BaseActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StartActivity.isFirst = false;
        super.onCreate(savedInstanceState);
        AppExitUtils.addActivity(this);
        AppExitUtils.setCurrentActivity(this);
    }

    @Override
    public void onResume() {
        AppExitUtils.setCurrentActivity(this);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        AppExitUtils.removeActivity(this);
        super.onDestroy();
    }

}
