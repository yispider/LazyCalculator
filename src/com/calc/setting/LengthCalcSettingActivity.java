package com.calc.setting;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.calc.R;

/**
 * Created by IntelliJ IDEA.
 * User: goblin
 * Date: 12-2-13
 * Time: 下午4:04
 * To change this template use File | Settings | File Templates.
 */
public class LengthCalcSettingActivity extends PreferenceActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.length_calc_preferences);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        // 更改了设置后需要立即生效的在这里面完成
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
                    @Override
                    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

                    }
                });
    }

}
